#include <pebble.h>

#define KEY_STATION 0
#define KEY_DEST_1 1
#define KEY_TIME_1 2
#define KEY_DEST_2 3
#define KEY_TIME_2 4
#define KEY_DEST_3 5
#define KEY_TIME_3 6
#define KEY_DEST_4 7
#define KEY_TIME_4 8
#define TIME_LABEL_LENGTH 5

static Window *window;
static TextLayer *station_text_layer;
static TextLayer *dest_text_layers[4];
static TextLayer *time_text_layers[4];
static char *time_strings[4];
static AppSync s_sync;
static uint8_t s_sync_buffer[128];

char *translate_error(AppMessageResult result) {
  switch (result) {
    case APP_MSG_OK: return "APP_MSG_OK";
    case APP_MSG_SEND_TIMEOUT: return "APP_MSG_SEND_TIMEOUT";
    case APP_MSG_SEND_REJECTED: return "APP_MSG_SEND_REJECTED";
    case APP_MSG_NOT_CONNECTED: return "APP_MSG_NOT_CONNECTED";
    case APP_MSG_APP_NOT_RUNNING: return "APP_MSG_APP_NOT_RUNNING";
    case APP_MSG_INVALID_ARGS: return "APP_MSG_INVALID_ARGS";
    case APP_MSG_BUSY: return "APP_MSG_BUSY";
    case APP_MSG_BUFFER_OVERFLOW: return "APP_MSG_BUFFER_OVERFLOW";
    case APP_MSG_ALREADY_RELEASED: return "APP_MSG_ALREADY_RELEASED";
    case APP_MSG_CALLBACK_ALREADY_REGISTERED: return "APP_MSG_CALLBACK_ALREADY_REGISTERED";
    case APP_MSG_CALLBACK_NOT_REGISTERED: return "APP_MSG_CALLBACK_NOT_REGISTERED";
    case APP_MSG_OUT_OF_MEMORY: return "APP_MSG_OUT_OF_MEMORY";
    case APP_MSG_CLOSED: return "APP_MSG_CLOSED";
    case APP_MSG_INTERNAL_ERROR: return "APP_MSG_INTERNAL_ERROR";
    default: return "UNKNOWN ERROR";
  }
}

char *translate_dict_error(DictionaryResult result) {
  switch (result) {
    case DICT_OK : return "DICT_OK";
    case DICT_NOT_ENOUGH_STORAGE : return "DICT_NOT_ENOUGH_STORAGE";
    case DICT_INVALID_ARGS : return "DICT_INVALID_ARGS";
    case DICT_INTERNAL_INCONSISTENCY : return "DICT_INTERNAL_INCONSISTENCY";
    case DICT_MALLOC_FAILED: return "DICT_MALLOC_FAILED";
    default: return "UNKNOWN ERROR";
  }
}


static void drawText() {
  const Tuple *tuple;
  if ((tuple = app_sync_get(&s_sync, KEY_STATION))) {
    if (tuple->value->cstring[0] == 0) {
      text_layer_set_text(station_text_layer, "Waiting for data");
    } else {
      text_layer_set_text(station_text_layer, tuple->value->cstring);
    }
  }
  for (int i = 0; i < 4; i++) {
    if ((tuple = app_sync_get(&s_sync, i * 2 + 1))) {
      if (tuple->value->cstring[0] == 0) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "No dest for row %d", i);
        text_layer_set_text(dest_text_layers[i], "");
      } else {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "Setting dest at row %d to %s", i, tuple->value->cstring);
        text_layer_set_text(dest_text_layers[i], tuple->value->cstring);
      }
    }
    if ((tuple = app_sync_get(&s_sync, i * 2 + 2))) {
      if (!tuple->value->int32) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "No time for row %d", i);
        text_layer_set_text(time_text_layers[i], "");
      } else {
        time_t departure_time = tuple->value->int32;
        time_t current_time = time(NULL);
        int minutes = (departure_time - current_time)/60;
        snprintf(time_strings[i], TIME_LABEL_LENGTH, "%dm", minutes);
        APP_LOG(APP_LOG_LEVEL_DEBUG, "Setting time at row %d to %s", i, time_strings[i]);
        text_layer_set_text(time_text_layers[i], time_strings[i]);
      }
    }
  }
}

static void sync_changed_handler(const uint32_t key, const Tuple *new_tuple, const Tuple *old_tuple, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Received sync changed event for key %u", (unsigned int)key);
  drawText();
}

static void sync_error_handler(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) {
  if (dict_error) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "App Sync Error: %s", translate_dict_error(dict_error));
  } else {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Error: %s", translate_error(app_message_error));
  }
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Select Click");
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Up Click");
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Down Click");
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}


static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  int16_t column2_width = 50;
  int16_t margin = 5;
  int16_t column1_width = bounds.size.w - column2_width - (margin * 2);
  station_text_layer = text_layer_create((GRect) { .origin = { 0, 0 }, .size = { bounds.size.w, 20 } });
  text_layer_set_text_alignment(station_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(station_text_layer));

  for (int i = 0; i < 4; i++) {
    dest_text_layers[i] = text_layer_create((GRect) { .origin = { margin, 30 * i + 20 }, .size = { column1_width, 30 } });
    text_layer_set_text_alignment(dest_text_layers[i], GTextAlignmentLeft);
    text_layer_set_font(dest_text_layers[i], fonts_get_system_font(FONT_KEY_GOTHIC_28));
    layer_add_child(window_layer, text_layer_get_layer(dest_text_layers[i]));
    
    time_text_layers[i] = text_layer_create((GRect) { .origin = { column1_width + margin, 30 * i + 20 }, .size = { column2_width, 30 } });
    text_layer_set_text_alignment(time_text_layers[i], GTextAlignmentRight);
    text_layer_set_font(time_text_layers[i], fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
    layer_add_child(window_layer, text_layer_get_layer(time_text_layers[i]));
    
    time_strings[i] = malloc(sizeof(char[TIME_LABEL_LENGTH]));
  }
}

static void window_unload(Window *window) {
  text_layer_destroy(station_text_layer);
  for (int i = 0; i < 4; i++) {
    text_layer_destroy(dest_text_layers[i]); 
    text_layer_destroy(time_text_layers[i]); 
    free(time_strings[i]);
    time_strings[i] = NULL;
  }  
}

static void init(void) {
  // setup AppSync
  app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
  Tuplet initial_values[] = {
    TupletCString(KEY_STATION, ""),
    TupletCString(KEY_DEST_1, ""),
    TupletInteger(KEY_TIME_1, 0),
    TupletCString(KEY_DEST_2, ""),
    TupletInteger(KEY_TIME_2, 0),
    TupletCString(KEY_DEST_3, ""),
    TupletInteger(KEY_TIME_3, 0),
    TupletCString(KEY_DEST_4, ""),
    TupletInteger(KEY_TIME_4, 0)
  };
  
  app_sync_init(&s_sync, s_sync_buffer, sizeof(s_sync_buffer), initial_values, ARRAY_LENGTH(initial_values), sync_changed_handler, sync_error_handler, NULL);
  
  // create window
  window = window_create();
  window_set_click_config_provider(window, click_config_provider);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);
}

static void deinit(void) {
  app_sync_deinit(&s_sync);
  window_destroy(window);
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
